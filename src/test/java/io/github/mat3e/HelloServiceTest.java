package io.github.mat3e;

import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class HelloServiceTest {
  //  private HelloService SUR = new HelloService();
    private final static String WELCOME = "Hello";
    private final static String FALBACK_ID_WELCOME= "Holala";


    @Test
    public void test_nullName_przekazanaNazwa() {
        var mockRepository = alwaysReturningHelloRepositoryNUR();
         var SUR = new HelloService(mockRepository);
        String name = null;
       // assertTrue(true);
        var rezulatat = SUR.przekazanaNazwa(null, "-1");
        assertEquals(WELCOME +  " " + HelloService.GDY_BRAK_NAME  + " !!!", rezulatat);
    }

    @Test
    public void test_name_przekazanaNazwa() {
        var  SUR = new HelloService();
        String name = "test";
        // assertTrue(true);
        var rezulatat = SUR.przekazanaNazwa(name, "-1");
        assertEquals(WELCOME + " " + name + " !!!", rezulatat);
    }


    @Test
    public void test_Name_nullLangle_przekazanaNazwa() {
      //  var falbackIdWelcome = "Holala";
        var mockRepository = new LangRepository(){
            @Override
            Optional<Lang> findByIDnur(Integer ID) {
                if(ID.equals((HelloService.longIDgreting.getId()))) {
                    return Optional.of(new Lang(null, FALBACK_ID_WELCOME, null));
                }
                return Optional.empty();
            }
        };

        var SUR = new HelloService(mockRepository);

        // assertTrue(true);
        var rezulatat = SUR.przekazanaNazwa(null, null);
        assertEquals(FALBACK_ID_WELCOME +  " " + HelloService.GDY_BRAK_NAME  + " !!!", rezulatat);
    }

    @Test
    public void test_brak_repo_przekazanaNazwa() {
        //  var falbackIdWelcome = "Holala";
        var mockRepository = new LangRepository(){
            @Override
            Optional<Lang> findByIDnur(Integer ID) {
                return Optional.empty();
            }
        };

        var SUR = new HelloService(mockRepository);

        // assertTrue(true);
        var rezulatat = SUR.przekazanaNazwa(null, "-1");
        assertEquals( HelloService.longIDgreting.getWelcomeMsg() +  " " + HelloService.GDY_BRAK_NAME  + " !!!", rezulatat);
    }

    @Test
    public void test_Name_TextLangle_przekazanaNazwa() {
        //  var falbackIdWelcome = "Holala";
        var mockRepository = new LangRepository() {
            @Override
            Optional<Lang> findByIDnur(Integer ID) {
                if (ID.equals((HelloService.longIDgreting.getId()))) {
                    return Optional.of(new Lang(null, FALBACK_ID_WELCOME, null));
                }
                return Optional.empty();
            }
        };

        var SUR = new HelloService(mockRepository);

        // assertTrue(true);
        var rezulatat = SUR.przekazanaNazwa(null, "abc");
        assertEquals(FALBACK_ID_WELCOME + " " + HelloService.GDY_BRAK_NAME + " !!!", rezulatat);
    }



        private LangRepository alwaysReturningHelloRepositoryNUR(){
        return new LangRepository(){  // dzieki temu tam gdzie jest rezozytorium
            // podstawimy nasze. Czyli jak by nie bylo łączności z repo to i tak nasze testy się udadza
            // bo podmieniamy te dane na nasze tu wstawione
            @Override
            Optional<Lang> findByIDnur(Integer ID) {
                // Optional.of bo go tworzymy wiec mamy pewosc ze bedzie i nie trzeba robic ofnull bo
                // mamy pewnosc ze nulem nie jest bo go zrobilismy :)
                return Optional.of(new Lang(null, WELCOME, null));
            }
        };
    }

}
