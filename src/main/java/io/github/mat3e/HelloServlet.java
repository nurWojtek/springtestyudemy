package io.github.mat3e;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet(name = "Hello", urlPatterns = {"/api/*"})
public class HelloServlet extends HttpServlet {
    private static final String NAME_PARAM = "name";
    private static final String LANG_PARAM = "lang";

    private final Logger logger = LoggerFactory.getLogger(HelloServlet.class);

    private HelloService serviceNur;

    /**
     * Nie uzywane potrzebne dla jetty (potrzebuje puste konstuktory aby uruchamiac serrvisy
     */
    @SuppressWarnings("nieuzywane")
    public HelloServlet()
    {
        this(new HelloService());
    }
    HelloServlet(HelloService serviceNur)
    {
        this.serviceNur=serviceNur;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("Got request with parameters: " + req.getParameterMap());
        var name = Optional.ofNullable(req.getParameter(NAME_PARAM)).orElse("world");
      //  String  name = req.getParameter(NAME_PARAM);
     //   System.out.printf("name = " + name);
     //   if(name.hashCode() == hashCode())
     //       name="slowo";
        var nazwa = req.getParameter(NAME_PARAM);
        var jezyk = req.getParameter(LANG_PARAM);
        resp.getWriter().write(serviceNur.przekazanaNazwa(nazwa, jezyk));
    }
}
