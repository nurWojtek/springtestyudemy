package io.github.mat3e;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class LangRepository {



 //   nie potrzebujemy mamy baze danych
   private List<Lang> langlist;
   LangRepository()
    {
        langlist = new ArrayList();
        langlist.add(new Lang(1, "Hello", "en"));
    //    langlist.add(new Lang(2, "Witaj", "pl"));
    }



    Optional<Lang> findByIDnur(Integer id)
    {
    //    juz nie potrzebujemy bo mamy Hibernate i baze
    //    return langlist.stream().filter(l -> l.getId().equals(id)).findFirst();  // filtrujemy i wywalamy piersze wystpapienie z danym id
        var sesion = HibernateUtil.getSessionFactory().openSession();
        var transaction = sesion.beginTransaction();
        var result = sesion.get(Lang.class, id);
        transaction.commit();
        sesion.close();
        return Optional.ofNullable(result);


    }
}
