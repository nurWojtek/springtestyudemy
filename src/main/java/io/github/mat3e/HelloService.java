package io.github.mat3e;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class HelloService {
    static final String GDY_BRAK_NAME = "Word";
    static final Lang  longIDgreting = new Lang (1, "Hello", "en");
    private final Logger logger = LoggerFactory.getLogger(HelloService.class);

    private LangRepository repository;

    HelloService(){
        this(new LangRepository());
    }

    HelloService(LangRepository repository){
        this.repository=repository;
    }

    /*String przekazanaNazwa(String name)
    {
        return  przekazanaNazwa(name, null);
    }
    */


    String przekazanaNazwa(String name, String longNUR){
        Integer longID;
        try {
            longID = Optional.ofNullable(longNUR).map(Integer::valueOf).orElse(longIDgreting.getId());  // przekaz nurll albo rzutuj stringa na longa albo zwroc to co wpisalismy na sztywno
        }
        catch (NumberFormatException e){
            logger.warn("langID powinno być typu LONG nie String " + longNUR);
            longID = longIDgreting.getId();
        }


            var welcomeMsg = repository.findByIDnur(longID).orElse(longIDgreting).getWelcomeMsg();  // weźmie albo z repo albo to co tu ustawiliśmy
        // na sztywno - Uniezaleznienie testow od działania repo
        var nameCostam = Optional.ofNullable(name).orElse(GDY_BRAK_NAME);
        return  welcomeMsg + " " + nameCostam + " !!!";
    }
}
