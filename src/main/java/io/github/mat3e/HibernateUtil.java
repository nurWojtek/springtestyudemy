package io.github.mat3e;

import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class HibernateUtil {
    //dzieki temu bedziemy mogli w nazedziach pozyskiwac SessionFactroy a przez to ze jest konstruktor prywatny nikt nic nie zmieni
    // nie ruszy itd a my dostajemy sie przez metode getSessonFactory
    // a wszystko jest static wiec istnieje bez konstruktora - taadammm
    // a cale SessonFactroy robi mam połącznienie z bazą danych. Fabryka sesji do zeby mozna bylo zapytac o cos baze danych oczywisce przez Hibernate :D


    private static  final  SessionFactory sessionFactory = buildSessionFactory();


    public static SessionFactory getSessionFactory(){
        return sessionFactory;
    }

    // statyczna bo chcemy uzyc razem z sessionFactory :D
    private static SessionFactory buildSessionFactory()  {
        // A SessionFactory is set up once for an application!
        final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure() // configures settings from hibernate.cfg.xml
                .build();
        try {
            return new MetadataSources( registry ).buildMetadata().buildSessionFactory();
        }
        catch (Exception e) {
            // The registry would be destroyed by the SessionFactory, but we had trouble building the SessionFactory
            // so destroy it manually.
            StandardServiceRegistryBuilder.destroy( registry );
            throw e;   // zwracamy wyjatek :D
        }
    }


    static void close()  {   // zamykamy cała SesionFactory
        if ( sessionFactory != null ) {
            sessionFactory.close();
        }
    }


    // prywatny konstruktor zrobi ze wszystko staje sie prywatne nikt nic nie moze zrobic no bo jak skoro konstruktor prywatny :)
    // nie da sie rownież dziedziczyć z tej klasy bo konstr prywatny a my dziedzicząc robimy jawnie albo za nas kompilator dodaje
       // kostruktor klasy bazowej :D
    private HibernateUtil(){

    }


}
